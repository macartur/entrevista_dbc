"""Module with tests."""

from unittest import TestCase

from src.data.usecases.combine_orders import LocalCombineOrders


class TestLocalCombineOrders(TestCase):
    """Suite of tests."""

    def test_combine_3_orders_with_n_max_100(self):
        """Test."""
        sut = LocalCombineOrders()
        params = {
            'orders': [70, 30, 10],
            'n_max': 100
        }
        result = sut.execute(params)
        expected_orders = 2
        self.assertEqual(result['orders'], expected_orders)

    def test_combine_1_order_with_n_max_100(self):
        """Test."""
        sut = LocalCombineOrders()
        params = {
            'orders': [70],
            'n_max': 100
        }
        result = sut.execute(params)
        expected_orders = 1
        self.assertEqual(result['orders'], expected_orders)

    def test_combine_4_orders_with_n_max_100(self):
        """Test."""
        sut = LocalCombineOrders()
        params = {
            'orders': [50, 70, 20, 10],
            'n_max': 100
        }
        result = sut.execute(params)
        expected_orders = 2
        self.assertEqual(result['orders'], expected_orders)

    def test_combine_6_orders_with_same_order(self):
        """Test."""
        sut = LocalCombineOrders()
        params = {
            'orders': [50, 70, 60, 70, 20, 10],
            'n_max': 100
        }
        result = sut.execute(params)
        expected_orders = 3
        self.assertEqual(result['orders'], expected_orders)

    def test_combine_orders_with_exactly_n_max(self):
        """Test."""
        sut = LocalCombineOrders()
        params = {
            'orders': [70, 30, 10, 90],
            'n_max': 100
        }
        result = sut.execute(params)
        expected_orders = 2
        self.assertEqual(result['orders'], expected_orders)

"""Module with tests."""

from unittest import TestCase

from src.domain.entities.contract import Contract
from src.data.usecases.get_top_n_open_contracts import \
        LocalGetTopNContracts


class TestLocalGetTopNOpenContracts(TestCase):
    """Suite of tests."""

    def test_get_top_3_open_contracts(self):
        """Test."""
        sut = LocalGetTopNContracts()
        contracts = [
                Contract(contract_id=1, debit=1),
                Contract(contract_id=2, debit=2),
                Contract(contract_id=3, debit=3),
                Contract(contract_id=4, debit=4),
                Contract(contract_id=5, debit=5),
        ]
        params = {
            'open_contracts': contracts,
            'renegotiated_contracts': [3],
            'top_n': 3
        }

        result = sut.execute(params)
        expected_open_contracts = [5, 4, 2]
        self.assertEqual(result['open_contracts'],
                         expected_open_contracts)

"""Module with top n open contracts."""
from abc import ABC, abstractmethod
from typing_extensions import TypedDict, List

from src.domain.entities.contract import Contract


class GetTopNContracts(ABC):  # pylint: disable=too-few-public-methods
    """Represents an usecase."""

    class Params(TypedDict):
        """Represents class params."""

        open_contracts: List[Contract]
        renegotiated_contracts: List[int]
        top_n: int

    class Result(TypedDict):
        """Represents class result."""

        open_contracts: List[int]

    @abstractmethod
    def execute(self, params: Params) -> Result:
        """Interface to get top N open contracts."""

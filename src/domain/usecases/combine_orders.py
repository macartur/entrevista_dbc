"""Module with combine order."""
from abc import ABC, abstractmethod
from typing_extensions import List, TypedDict


class CombineOrders(ABC):
    """Represents a combine orders usecase."""

    class Params(TypedDict):
        """Params."""

        orders: List[int]
        n_max: int

    class Result(TypedDict):
        """Result."""

        orders: int

    @abstractmethod
    def execute(self, params: Params) -> Result:
        """Combine orders."""

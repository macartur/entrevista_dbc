"""Module with the entities."""
from typing_extensions import TypedDict


class Contract(TypedDict):
    """Class to represent a business contract."""

    contract_id: int
    debit: int

    def __str__(self):
        """Display a contract as string."""
        return f'id={self.contract_id}, debit={self.debit}'

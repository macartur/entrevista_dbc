"""Module with GetTopNOpenContracts usecase."""
from src.domain.usecases.get_top_n_open_contracts import GetTopNContracts


class LocalGetTopNContracts(GetTopNContracts):
    """Represents an usecase."""

    @staticmethod
    def order_by_key(element):
        """Get ordered information."""
        return element['debit']

    def execute(self, params):
        """Interface to get top N open contracts."""
        open_contracts = []

        for contract in params['open_contracts']:
            if contract['contract_id'] not in params['renegotiated_contracts']:
                open_contracts.append(contract)

        open_contracts.sort(reverse=True, key=self.order_by_key)
        contracts_id = [contract['contract_id'] for contract in
                        open_contracts[:params['top_n']]]

        return GetTopNContracts.Result(open_contracts=contracts_id)

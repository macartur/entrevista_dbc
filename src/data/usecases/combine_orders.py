"""Module with combine order usecase."""
from src.domain.usecases.combine_orders import CombineOrders


class LocalCombineOrders(CombineOrders):
    """Class to combine orders locally."""

    def execute(self, params: CombineOrders.Params) -> CombineOrders.Result:
        """Combine orders."""

        count = 0

        sorted_list = params['orders'].copy()
        sorted_list.sort()

        while len(sorted_list) > 0:
            if len(sorted_list) == 1:
                sorted_list.pop()
                count += 1
            elif sorted_list[0] + sorted_list[-1] <= params['n_max']:
                sorted_list.pop()
                sorted_list.pop(-1)
                count += 1
            else:
                sorted_list.pop()
                count += 1

        return CombineOrders.Result(orders=count)

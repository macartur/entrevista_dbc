# Entrevista DBC

Para a entrevista eu utilizei as seguintes tecnicas:

## Arquitetura Limpa

Utilizei uma arquitetura limpa, que eu aprendi com o curso rodrigo manguinho.
Nesta arquitetura dividimos a arquiteura em 5 partes domain, data, infra, presentation, view.
Neste projeto estou utilizando apenas o domain e data. Nesta duas camadas é apresentado a assinatura dos casos de uso,
e a entidade que será usada em nosso sistema.
Caso fosse um projeto maior poderiamos separar todas funcionalidades e as dependências seguindo estas camadas.

## Usecases

Criei a assinatura do método CombineOrders e GetTopNOpenContracts para serem os casos de uso.
Para cada contrato fiz uma interface de Contrato que representa um dicionario com todas as informações de um contrato.

## TDD

Fiz a implementação utilizando TDD e criei alguns testes para assegurar que das proximas vezes que este seja
modificado não altere o comportamento do que ja foi implementado.

## GIT
Para o git estou seguindo o git-commit-msg-linter, este é especificado no seguinte link: https://www.conventionalcommits.org/en/v1.0.0-beta.3/
